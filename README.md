# tool

1. Napisz swój pierwszy program Hello World
2. Podstawy składni języka

    Interpreter PHP wymaga, aby kod PHP był osadzony w nastepujących znacznikach <?php ?>. To mówi interpreterowi, Że wszystko co znajduje się w środku ma być traktowane jako kod.

    W PHP każda instrukcja musi byc zakończona średnikiem
    <?php echo "Hello World"; ?>
    Jeśli plik zawiera tylko PHP, domukajacy tag ?> nie jest wymaganey. To mówi interpreterowi, że cały plik ma byc wykonany jako kod PHP

    W kodzie można umieszczać komentarze, które opisują co robi kod. Komentarze nie są "wykonywane", więc można tam napisać wszystko.co chcesz. Sposoby osadzania komentarzy:
    ```php
    // to jest komentarz
    /* to jest komentarz */
    /*
    * To też jest komentarz
    * tym razem wielolonijkowy
    */
    ```

3. Zmienne:
    Zmienna to konstrukcja programistyczna, którą cechuja 3 atrybyty:
        - nazwa
        - wartość
        - miejsce przechowywania (zwykle pamięć komputera) i tym się nie zajmujemy obecnie. Po prostu miej świadomość, że komputer przydzieli zmiennnej adres wa pamięci

    Aby stworzyc zmienną w PHP, trzeba użyć znaku dolara $
    i nastepnie bezpośrednio za nim wpisac nazwę zmiennej. Tworząc zmienną chcemy zazwyczaj, aby posiadała jakąś wartość. Robimy to poprzez POJEDYNCZY znak równości "=" po nazwie zmiennej.
    Przykład utworzenia zmiennej (poniższe 4 zapisy są tożsame):
    ```php
    $test=1;
    $test =1;
    $test= 1;
    $test = 1;
    `
    Niezaleznie od sposobu zapisu, który na w tym przypadki znaczenie jedynie wizualne, utworzona została zmienna test i przypisana do niej wartość 1
    ```php
    $test = "Jakaś treść";
    ```

    Analogicznie, utworzona została zmienna test, ale tym razem przypisany został do niego ciag znaków Jakaś treśc.
    Zwróc uwagę na uzyte cudzysłowa, których nie było, gdy tworzyłes zmienną test i przypisywałes jej wartośc 1.
    To kolejny rozdział, czyli typu (lub inaczej rodzaje) zmiennych.

    4. Typy (rodzaje) zmiennych
    Generalnie wyrózniamy 2 typy zmiennych
        - proste
        - złożone

    Typy okreslają jaką wartość przechowuje zmienna i tak:
    Typy proste to:
        - boolean - prawda lub fałsz.
            $test = true;
        - integer - liczby całkowite (0, 1, 2, 564)
            $test = 345;
        - double - liczby rzeczywiste (1.0, 1.00, 10.999)
            $test = 1.5
        - string - ciagi znaków
            $test = "tekst";
            $test = 'tekst';

      Z typów złożonych na napotrzeby tej nauki,wymienię tylko jeden:
        - array - tzw. typ tablicowy

  4. Proste operatory arytmetyczne
    $a + $b - dodaj zmienna a do b
    $a - $b - dodaj zmienna b do a
    $a * $b - pomnóz a przez b
    $a / $b - podziel a przez b
    $a % $b - podziel modulo a przez b. Wynikiem jest tzw reszta z dzielenia. np.
        6 % 2 = 0 // nie ma reszty
        5 % 2 = 1 // jest reszta


  5. Operatory porównawcze
    - == (równe)
    - === (identyczne)
    - != (nierówne)
    - !== (nieidentyczne)
    - < (mniejsze niż)
    - > (większe niż)
    - <= (mniejsze równe)
    - >= (większe równe)


   6. Instrukcje warunkowe

   ```php
   // Jeśli coś
   if() {
       // to wtedy coś
   }

   // Jeśli coś
   if() {
       // to wtedy coś
   }
   else{
       // w przeciwnym wypadku cos innego
   }

   // Jeśli coś
   if() {
       // to wtedy coś
   }
   elseif(){ //
       // jeśli nie to co wyżej, to coś
   }
   else{
       // jesli żadne w powyższych, to coś jeszcze innego
   }
   ```
   ```php

   $a = 4;

   if($a == 4) {
       echo "Zmienna a ma wartość $a";
   }

   if($a < 5) {
       echo "Zmienna a jest mniejsza od 5";
   }
   else{
       echo "Zmienna a nie jest mniejsza od 5";
   }

   if($a > 4) {
       echo "Zmienna a jest większa od 4";
   }
   elseif($a < 4){
       echo "Zmienna a jest mniejsza od 4";
   }
   else{
       echo "Logiczne jest, że zmienna a ma wartość 4"
   }
   ```
  7. Funkcje.
    Funkcja to przyporządkowanie każdemu elementowi zbioru X dokładnie jednego elementu zbioru Y. Znasz na pewno zapis:
    f(x)=x=2
    W programowaniu jest podobnie, ale funkcje wykorzystuje się czesto do przechowywania jakichś algorytmów
