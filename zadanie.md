# Zadanie

Napisz funkcję, która sprawdza, czy podana jako argument liczba
jest parzysta. Funkcja powinna zwrócić true lub false.

Następnie wykorzystując istniejącą funkcję, napisz instrukcje wyświetlające napis "parzysta" lub "nieparzysta" dla następujących zmiennych
```php
$a = 2;
$b = 5;
$c = "7";
$d = 8.6;
```
Każdy wynik powinien być w osobnej linii. Uzasadnij otrzymany wynik dla $d

protip 1:
Aby wyświetlac wyniki w osobnych linijkach, musisz dokleić znak końca lini. W htmlu, znakiem końca lini jest ```"<br>"```, np.
```php
echo "jakiś tekst<br>kolejny tekst";
```
protip 2:
W instrukcji echo możesz "sklejać" zmienne i treść stosując znak konkatacji, którym jest kropka.
Przykład:
```php
$a = 1;
echo "Wartość zmiennej a wynosi " . $a;
```
Wyświetli: Wartość zmiennej a wynosi 1;
