# Zadanie

Zadania 1

Masz zadaną tablicę liczb
```php
$numbers = [7, 18, 3, 67, 43, 98, 2];
```
Uporządkuj wartości w tablicy w kolejności rosnącej.
W zadaniu pomoże jedna z funkcji ze strony https://www.php.net/manual/en/array.sorting.php



```php
function listNumbers($array){
    $count = count($array);
    for($i=0, $i<$count; $i++){
        echo $array[$i].'<br>';
    }
}


$numbers = [7, 18, 3, 67, 43, 98, 2];

// Tutaj pisz kod


// To zostaw. To jest
listNumbers($numbers);

```

Zadanie 2

Używając oryginalnej tablicy liczb z zadania 1, oblicz sumę tych liczb
i wyświetl ją na ekranie. Zrób to na dwa sposoby:
    a) stwórz własny algorytm sumujący
    b) wykorzystaj jedną zw wbudowanych funkcji. Aby znaleźc rzeczoną funkcję użyj google lub https://www.php.net/manual/en/ref.array.php
