<?php
function isNumberOdd(int $number) : bool {
    return $number % 2;
}

function getNumberTypeText(int $number) : string {
    $texts = ["even", "odd"];
    return 'Number ' .$number. ' is ' . $texts[(int)isNumberOdd($number)];
}

$numbers = [2, 5, "7", 8.6, "test"];
foreach($numbers as $number){
    try{
        echo getNumberTypeText($number) . '<br>';
    }
    catch(TypeError $e){
        echo $number .' is not numeric';
    }
}
