<?php
if(!(2 % 2)){
    print "ok";
}
function listNumbers($array){
    $count = count($array);
    for($i=0, $i<$count; $i++){
        echo $array[$i].'<br>';
    }
}

// Tutaj pisz kod
$numbers = [7, 18, 3, 67, 43, 98, 2];

listNumbers($numbers);
die();
$ignore = false;

for($i=1; $i<=5; $i++){
    if($ignore == false && $i==3){
        break;
    }
    echo $i.'<br>';
}

for($i=10; $i>=1; $i--){
    echo $i.'<br>';
}



function isNumberOdd($number){
    return (bool)($number % 2);
}

function getNumberTypeText($number){
    $string = 'Number ' .$number. ' is ';
    if(isNumberOdd($number)){
        $string .= 'odd';
    }
    else{
        $string .= 'even';
    }
    return $string.'<br>';
}

function getNumberTypeTextBetterWay($number){
    $texts = ["even", "odd"];
    return 'Number ' .$number. ' is ' . $texts[(int)isNumberOdd($number)] ;
}

// Sposób 1
$a = 2;
$b = 5;
$c = "7";
$d = 8.6;
echo getNumberTypeText($a);
echo getNumberTypeText($b);
echo getNumberTypeText($c);
echo getNumberTypeText($d);

//Sposób 2
$numbers = [2, 5, "7", 8.6];
foreach($numbers as $number){
    echo getNumberTypeText($number);
}

// $toolSongs1 = ["Lateralus", "Jambi", "Pneuma"];
// $toolSongs2 = [
//     0=>"Lateralus",
//     1=>"Jambi",
//     2=>"Pneuma"
// ];
// var_dump($toolSongs1);
// var_dump($toolSongs2);


// echo count($toolSongs1);
// for($i = 0; $i < count($toolSongs1); $i++){
//     echo $i . '<br>';
// }

// $i = 10;
// while($i < 10){
//     echo $i . '<br>';
//     $i++;
// }
