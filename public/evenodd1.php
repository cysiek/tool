<?php
function isNumberOdd($number){
    if($number % 2){
        return true;
    }
    return false;
}

function getNumberTypeText($number){
    $string = 'Number ' .$number. ' is ';
    if(isNumberOdd($number)){
        $string .= 'odd';
    }
    else{
        $string .= 'even';
    }
    return $string.'<br>';
}

$a = 2;
$b = 5;
$c = "7";
$d = 8.6;
echo getNumberTypeText($a);
echo getNumberTypeText($b);
echo getNumberTypeText($c);
echo getNumberTypeText($d);
