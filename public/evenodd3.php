<?php
function isNumberOdd($number){
    return (bool)($number % 2);
}

function getNumberTypeText($number){
    $texts = ["even", "odd"];
    return 'Number ' .$number. ' is ' . $texts[(int)isNumberOdd($number)];
}

$numbers = [2, 5, "7", 8.6];
foreach($numbers as $number){
    echo getNumberTypeText($number) . '<br>';
}
