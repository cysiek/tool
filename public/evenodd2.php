<?php
function isNumberOdd($number){
    return (bool)($number % 2);
}

function getNumberTypeText($number){
    $texts = ["even", "odd"];
    return 'Number ' .$number. ' is ' . $texts[(int)isNumberOdd($number)] . '<br>';
}

$a = 2;
$b = 5;
$c = "7";
$d = 8.6;
echo getNumberTypeText($a);
echo getNumberTypeText($b);
echo getNumberTypeText($c);
echo getNumberTypeText($d);
